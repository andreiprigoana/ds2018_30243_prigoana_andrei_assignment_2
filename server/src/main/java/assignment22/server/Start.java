package assignment22.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import assignment22.common.OperationService;

public class Start {

	public static void main(String[] args) {

		try {
			OperationService service = new OperationServiceImplementation();
			OperationService stub = (OperationService) UnicastRemoteObject.exportObject(service, 0);
			Registry registry = LocateRegistry.createRegistry(1099);
			registry.rebind("OperationService", stub);
			System.out.println("Server started.");
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

}

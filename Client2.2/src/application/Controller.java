package application;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import assignment22.common.Car;
import assignment22.common.OperationService;

public class Controller implements ActionListener {

	private View view;

	private Car car;

	public Controller() {
		view = new View();

		this.view.addItemListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getB1()) { // Compute Tax
			car = new Car(Integer.parseInt(view.getF1().getText()), Integer.parseInt(view.getF3().getText()));

			try {
				Registry registry = LocateRegistry.getRegistry(1099);
				OperationService service = (OperationService) registry.lookup("OperationService");
				double result = service.computeTax(car);
				view.getF4().setText(String.valueOf(result));
			} catch (Exception exception) {
				System.out.println(exception);
			}
		} else if (e.getSource() == view.getB2()) { // Compute Selling Price
			car = new Car(Integer.parseInt(view.getF1().getText()), Double.parseDouble(view.getF2().getText()),
					Integer.parseInt(view.getF3().getText()));

			try {
				Registry registry = LocateRegistry.getRegistry(1099);
				OperationService service = (OperationService) registry.lookup("OperationService");
				double result = service.computeSellingPrice(car);
				view.getF4().setText(String.valueOf(result));
			} catch (Exception exception) {
				System.out.println(exception);
			}
		}

	}
}

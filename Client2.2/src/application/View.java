package application;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View {

	private JFrame frame;

	private JPanel p1;

	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;
	private JLabel label5;

	private JButton b1;
	private JButton b2;

	private JTextField f1;
	private JTextField f2;
	private JTextField f3;
	private JTextField f4;

	public View() {
		frame = new JFrame("Services");
		frame.setBounds(500, 200, 550, 400);

		p1 = new JPanel();
		frame.add(p1);

		p1.setLayout(new BorderLayout());

		b1 = new JButton("Compute Tax");
		b2 = new JButton("Compute Selling Price");

		f1 = new JTextField(); // ec
		f2 = new JTextField(); // price
		f3 = new JTextField(); // year
		f4 = new JTextField(); // result

		label1 = new JLabel("Engine Capacity:");
		label2 = new JLabel("Price:");
		label3 = new JLabel("Year:");
		label4 = new JLabel("Result:");
		label5 = new JLabel();

		b1.setBounds(300, 250, 200, 25);
		p1.add(b1);
		b2.setBounds(60, 250, 200, 25);
		p1.add(b2);

		f1.setBounds(180, 120, 200, 25);
		p1.add(f1);
		f2.setBounds(180, 160, 200, 25);
		p1.add(f2);
		f3.setBounds(180, 200, 200, 25);
		p1.add(f3);
		f4.setBounds(180, 300, 200, 25);
		p1.add(f4);

		label1.setBounds(70, 120, 200, 25);
		p1.add(label1);
		label2.setBounds(100, 160, 200, 25);
		p1.add(label2);
		label3.setBounds(100, 200, 200, 25);
		p1.add(label3);
		label4.setBounds(100, 300, 200, 25);
		p1.add(label4);

		p1.add(label5);
		frame.setVisible(true);
	}

	public void addItemListener(ActionListener a) {
		b1.addActionListener(a);
		b2.addActionListener(a);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getP1() {
		return p1;
	}

	public void setP1(JPanel p1) {
		this.p1 = p1;
	}

	public JLabel getLabel1() {
		return label1;
	}

	public void setLabel1(JLabel label1) {
		this.label1 = label1;
	}

	public JLabel getLabel2() {
		return label2;
	}

	public void setLabel2(JLabel label2) {
		this.label2 = label2;
	}

	public JLabel getLabel3() {
		return label3;
	}

	public void setLabel3(JLabel label3) {
		this.label3 = label3;
	}

	public JLabel getLabel4() {
		return label4;
	}

	public void setLabel4(JLabel label4) {
		this.label4 = label4;
	}

	public JLabel getLabel5() {
		return label5;
	}

	public void setLabel5(JLabel label5) {
		this.label5 = label5;
	}

	public JButton getB1() {
		return b1;
	}

	public void setB1(JButton b1) {
		this.b1 = b1;
	}

	public JButton getB2() {
		return b2;
	}

	public void setB2(JButton b2) {
		this.b2 = b2;
	}

	public JTextField getF1() {
		return f1;
	}

	public void setF1(JTextField f1) {
		this.f1 = f1;
	}

	public JTextField getF2() {
		return f2;
	}

	public void setF2(JTextField f2) {
		this.f2 = f2;
	}

	public JTextField getF3() {
		return f3;
	}

	public void setF3(JTextField f3) {
		this.f3 = f3;
	}

	public JTextField getF4() {
		return f4;
	}

	public void setF4(JTextField f4) {
		this.f4 = f4;
	}

}

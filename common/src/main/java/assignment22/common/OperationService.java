package assignment22.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface OperationService extends Remote{

	double computeTax(Car c) throws RemoteException;
	
	double computeSellingPrice(Car c) throws RemoteException;
}

package assignment22.common;

import java.io.Serializable;

public class Car implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int year;
	private int engineCapacity;
	private double price;

	public Car() {
	}

	public Car(int engineCapacity, double price, int year) {
		this.year = year;
		this.engineCapacity = engineCapacity;
		this.price = price;
	}
	
	public Car(int engineCapacity, int year) {
		this.year = year;
		this.engineCapacity = engineCapacity;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(int engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [year=" + year + ", engineCapacity=" + engineCapacity + ", price=" + price + "]";
	}
	
	
	
}
